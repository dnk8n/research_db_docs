---
title: Search Strategy
---
The entry for the database is temporarily limited to a set of selection criteria to test its comparative potential and added value for researchers from African countries, for science policy scholars as well as for policy-makers. 

1. The first criteria is that researchers and research-support staff members from all African countries at all stages of their academic career are eligible, either individually or as collaborators in project teams.
2. The second criteria are a pre-selected group of 50 funding organizations (as of 16.8.2020). The selection of these organizations follows two criteria:

   * The funding organizations have been identified in recent bibliometric analyses and surveys among African scholars as the most frequently listed funding organisations ([Kozma, Medina, und Costas 2018; Mouton, Prozesky, und Lutomiah 2018](https://www.africanminds.co.za/the-next-generation-of-scientists/)).
   * African public and private research funding organizations from 20 countries are included in the search pattern. These 20 countries were identified as the most productive ones of 54 in regard to scientific publication outputs.

     Searching for funding programs from African funding organizations often led to the inclusion of further funding organisations outside the pre-selected 50 organizations. Frequent bilateral funding programs account for this necessary expansion of funding organizations.
3. The priority is funding for research projects and research programs of groups and individuals (Usage). 

   * Research projects of individuals include funding programs such as research chairs, prestigious grants and similar funding programs.
   * Research funding is differentiated from research positions such as lectureships, professorships and similar regular staff position. There are numerous service providers offering overviews of open positions. Grants for a limited time such as research chairs are an exception and are included into this database.
4. Funding programs need to be available for postdoctoral scholars and above (academic position). Therefore, we temporarily also exclude funding opportunities for individual PhD-applicants. However, PhD-funding can be part of collaborative research funding and included into the database in the future.