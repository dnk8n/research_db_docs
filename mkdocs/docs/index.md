---
title: "Short Description of the Funding Observatory "
---
<script src="https://identity.netlify.com/v1/netlify-identity-widget.js"></script>

This is the documentation for an ***Open Data Funding Observatory*** created for the purpose of collecting information about funding for which African scholars are eligible. At the beginning the database focuses on opportunities for global and local research collaborations. It is planned to expand the scope of the database. 

The content is freely available and adresses research and science policy communites. After the Minimal Version we plan to develop and install an API for users to download and search the data.

This documentation gives you an overview of the database's structure and search strategy. 

Feel free to contact us for questions and comments: funding-observatory@sureco-review.net

### Why is this topic relevant?

* Scientific collaborations and their funding mechanisms are viable instruments for global sustainable development as emphasized in Goal 4, 9 and 17 of the Sustainable Development Goals.
* However, research shows that science cooperation does not always work out well, especially when structured by persistent North-South asymmetries. Such asymmetries can affect the agenda-setting, the financing mechanisms and the division of labor within the research process.
* Therefore, the design of funding programs matters to the success of research collaborations. 
* While there are funder’s evaluations of single funding schemes and projects as well as available data of single funding programs, so far, no study seems to profoundly compare the results and insights as well as the hypotheses of agenda-setting.

### What are the observatory's objectives?

* The database will collect information of funding programs that are open to African researchers.
* The results help placing national public and private funding initiatives into a wider context of continuous international and national science funding in and for African countries.
* The results permit to reflect on the expectations by various stakeholders and to discuss the necessary means and designs to counter North-South asymmetries for more sustainable research cooperation.
* The open funding observatory will be one instrument among others to shed light on the process of research funding, offering interoperability through the use of harmonized institutional identifiers (GRID) for further analysis.

### Who are the potential users?

* The database contains data that is of interest for the sociology and policy of science-community. It will be continuously expanded in future projects and can serve as open data for research and teaching of 
  international science policy with specific focus on research funding.
* Equally, the study can inform funding agencies, science policy-makers and a wider audience about trends, patterns and the self-understanding of a growing network of global players.
* The database can serve as an open-access knowledge hub for grant-seekers in the long run. 
* To remain an up-to-date database we seek collaborations with other collectors of funding programs for African scholars in order to share the database and its functionalities as a technique and to share the data for more robust and comprehensive overviews of funding policies by African and non-African research funders. 

### What is open about the funding observatory?

* The form the collected data is presented is freely available and will eventually be approachable through an API for collaboration and further use of data. 
* Also the database's structure and its import-mechanisms itself is publicly available under the GNU Affero General Public License v3.0.: [gitlab-repository](https://gitlab.com/dnk8n/research_db)

### Who is behind the observatory?

Stefan Skupien developed the idea and publication strategy during a research project on African-European Scientific Collaboration and tested a first minimal public database on his blog [sureco-review.net](https://sureco-review.net) from 2017 to 2019 ([ORCID](https://orcid.org/0000-0001-8519-8738), [researchgate](https://www.researchgate.net/profile/Stefan_Skupien)). Dean Kayton joined the project in 2020 and created the new database and API ([Website](dnk8n.me), [Gitlab](https://gitlab.com/dnk8n)). Laura Mareski and Kahindo Kyakimwa were crucial in testing and filling the database from August to September 2020. Johanna Havemann is continuously contributing as a mentor ([Access2Perspectives](https://access2perspectives.com), [Preprint-Service AfricArxiv](info.africarxiv.org)). In March 2021, Charles Yusuf joined the team to develop the front-end ([www.freemanslab.com](www.freemanslab.com)).

Initial funding was provided by Volkswagen Foundation for a research project on African-European Scientific Collaboration from 2016-2019 (Link). A 2019/20-fellowship from the Open Science Fellows Program (Wikimedia/Stifterverband/Volkswagen Foundation) provided support to build the database and to employ student assistants for first test runs ([Link](https://en.wikiversity.org/wiki/Wikimedia_Deutschland/Open_Science_Fellows_Program)).