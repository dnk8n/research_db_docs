---
title: Database Structure
---
The structure adapts the model of the funding database SARECO ([Link](https://sareco.ch/funds)).

The database contains 10 tables, which are described and defined in detail below.

#### Academic Position

The Academic Position describes the targeted person or group of persons of each funding program. There can be multiple positions offered in one funding program.

* Consortium – a group of researchers and/or research-support staff members planning to conduct research together
* Researcher – where no other positions for individuals are clarified use this category
* Postdoctoral researcher – the persons holds a PhD at the moment of application or has submitted his/her PhD-thesis
* PhD-candidate – the persons is about to start or is currently pursuing his/her PhD-studies.
* Graduate student – the person is about to start or is currently pursuing his/her Master-Degree or any other post-Bachelor degree
* Undergraduate student – the person is about to start or is currently pursuing his/her first tertiary degree (e.g. Bachelor).

#### Scientific Disciplines

The category describes the scientific disciplines and is currently copied from the OECD list of scientific discipline categories ([Link](http://www.oecd.org/science/inno/38235147.pdf)). E.g. a funding program can be limited to one or few scientific disciplines or can be open to all disciplines. (multiple selections possible)

#### Countries

The list contains all countries and their Alpha 2 and Alpha 3 Codes \[not changable] The countries will be selected in the table 'Funding Programs' to designate countries of origin or residence of applicants. (multiple selection possible)

Funding organizations often cluster countries to differentiate between different groups from where applicants can come and to limit eligibility. Such groups can be summarized in one keyword to save time when adding funding programs. A list of keywords will be provided here in the future.

#### Funding organisation and implementing organisation

The table contains the basic information of organizations.  Funding organizations include all organizations that issue a funding program and sustain it financially (e.g. foundations, national science ministries, national research foundations).

Implementation organizations include all organizations that manage a funding program (e.g. the African Academy of Sciences develops and manages grants that are funded by the Wellcome Trust and others). Universities can be implementing organizations if a separate funding organization is mentioned. 

Basic information of funding or implementing organizations are retrieved from the GRID-inventory of research and funding organizations. In case there is no GRID-ID available for a specific funding or implementing organization, an internal reference will be created \[see below] and used as long as there is no GRID-ID (it is planned to forward missing values on organizations to GRID). 

#### Funding program

**Working definition**: A funding program is an activity by an organization meant to distribute resources to reach a designated goal in a competitive manner. Such organizations include scientific research councils, state ministries and private foundations. Goals can include but are not limited to academic education, individual and collective research projects, short- and longterm mobility of researchers, scientific events such as workshops and institutional support to strengthen scientific ecosystems.

The database will neither store information on the level of funded projects nor on the level of organizational strategies. However, we plan to include information on evaluations and relevant policy-documents directly related to funding programs in the future.

This table is important in at least two regards: First, the table registers one to multiply funders and can later be analyzed for patterns of multi-donor cooperation. Secondly, the registration of selection criteria and motivation facilitates the analysis of funder’s discourses and their justification across scientific and other audiences.

#### GRID References

Grid References contain information about organizations that are related to academy, funding, research policy and other research related topics. Any organization can become a funding or implementing organization in the definition of this funding observatory if it is related to a funding program. For instance, a university can fund international collaborative research from its own resources = funding organization. A university can also become an implementing organization to manage acquired resources = implementing organization. 

This table is updated automatically and retrieves information from the GRID database ([Link](https://www.grid.ac/)). 

This funding observatory uses GRID references to provide a coherent description of funding and implementation organizations and to assure openness and connectivity for further analytical analyses.

#### Internal References

If there is yet no GRID reference for an organization available, internal references will be created following the model of the GRID reference scheme. These internal references will be used as long as the GRID database does not contain information about the organization.

#### Keywords

This table contains keywords attributed to funding programs. Keywords should not repeat information that is already captured by the other sub-tables. An exception are keywords taken from titles and funding program descriptions. 

#### Thematic Focuses

This section on thematic focuses includes prominent topics and should be selected from the program's description (e.g. Good Governance, Infectious Diseases, Social Welfare, Sustainable Development Goals, Climate Change).

The list will be curated and harmonized after regular periods of entering information from funding programs.

#### Usage

The field ‘Usage’ captures the purpose of funding. It contains research, business and innovation, mobility, events and publications. The purpose is taken from the funding program’s description. Multiple entries for one funding program are possible.