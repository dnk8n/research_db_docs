#!/usr/bin/env bash

repo_dir="$(dirname "$0")";
set -x
cd "${repo_dir}/mkdocs"
docker run --rm -it -p ${DOCS_PORT:-8000}:${DOCS_PORT:-8000} -v ${PWD}:/docs squidfunk/mkdocs-material serve --dev-addr=0.0.0.0:${DOCS_PORT:-8000}
