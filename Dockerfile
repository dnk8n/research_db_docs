FROM squidfunk/mkdocs-material as builder
COPY mkdocs /docs
RUN mkdocs build

FROM caddy:2.1.1-alpine as server
RUN rm -rf /usr/share/caddy
COPY --from=builder /docs/site /usr/share/caddy
