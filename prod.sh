#!/usr/bin/env bash

repo_dir="$(dirname "$0")";
set -x
cd "${repo_dir}"
git pull
docker-compose up -d --build
