# Contributing

## Quick Start

- Add/Edit markdown files in `./mkdocs/docs` directory of this repo
- When changes are pushed to the master branch, automation will regenerate the production static web page
- It is possible to make edits in browser via Gitlab (in markdown format) or via NetlifyCMS (in markdown or rich text format)
- For more advanced features, [see mkdocs documentation](https://www.mkdocs.org)

## Developing

- Often it is acceptable (with caution) to make a change directly to the master branch if it is something simple like editing markdown
- For something more complex, it is advisable to run a development server locally
- There are convenience scripts included (where setting the `DOCS_PORT` variable (default shown) is optional) to:
  - run a development server, e.g. `DOCS_PORT=8000 ./dev.sh`
  - simulate the production server, e.g. `DOCS_PORT=80 ./prod.sh` (to override other shell variables follow the instructions within .env.tpl)
- The official Docker image for mkdocs-material is used, [see mkdocs-material documentation on Dockerhub](https://hub.docker.com/r/squidfunk/mkdocs-material/)
- For more general information about the theme, [see mkdocs-material main documentation](https://squidfunk.github.io/mkdocs-material)

